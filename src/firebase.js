import firebase from 'firebase'


// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyD5YSZJiV94cHiioG7buV1IEt0rKE_b1p8",
    authDomain: "ecommerce-ab6d9.firebaseapp.com",
    projectId: "ecommerce-ab6d9",
    storageBucket: "ecommerce-ab6d9.appspot.com",
    messagingSenderId: "1048659664981",
    appId: "1:1048659664981:web:4e6514e684a7d025ebb4dc"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

//export
export const auth = firebase.auth();
export const googleAuthProvider = new firebase.auth.GoogleAuthProvider();